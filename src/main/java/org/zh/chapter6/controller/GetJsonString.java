package org.zh.chapter6.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/c6")
public class GetJsonString {
    @RequestMapping("getJsonString")
    public String getJsonString(@RequestParam("jsonStr") String jsonStr, Model model){
        JSONObject object = JSONObject.parseObject(jsonStr);
        System.out.println("username-"+object.getString("username"));
        System.out.println("password-"+object.getString("password"));
        model.addAttribute("username",object.get("username"));
        return "sendAjax";
    }
}
