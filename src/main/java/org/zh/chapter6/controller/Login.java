package org.zh.chapter6.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/c6")
public class Login {
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(String username,String password,RedirectAttributes attr) {
        attr.addFlashAttribute("username", username);
        if (username.equals("gzh") && password.equals("qxh")) {
            System.out.print(username);
            return "redirect:listUsername";
        }else{
            return "redirect:listUsername1";
        }
    }

    @RequestMapping(value = "/listUsername")
    public String listUsername(@ModelAttribute("username") String username, Model model) {
        List<String> listUsername = new ArrayList<String>();
        for (int i = 0; i < 6; i++) {
            listUsername.add(username + i);
        }
        model.addAttribute("username",username);
        model.addAttribute("listUsername",listUsername);
        return "/listUser";
    }

    @RequestMapping(value = "/listUsername1")
    public ModelAndView listUsername(HttpServletRequest request) {
        List<String> listUsername = new ArrayList<String>();
        Map<String,String> params = (Map<String,String>) RequestContextUtils.getInputFlashMap(request);
        String username = params.get("username");
        for (int i = 0; i < 6; i++) {
            listUsername.add(username + i);
        }
        ModelAndView modelView = new ModelAndView("listUser1");
        modelView.addObject("username",username);
        modelView.addObject("listUsername",listUsername);
        return modelView;
    }
}
