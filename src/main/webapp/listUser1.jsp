<%@page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/common/taglibs.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>spring mvc step by step</title>
</head>
<body>
<form>
    Hello,${username}!<br/>
    <c:forEach var="username" items="${listUsername}">
        ${username}<br/>
    </c:forEach>
</form>

</body>
</html>