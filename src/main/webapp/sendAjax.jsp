<%@page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/common/taglibs.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <title>spring mvc step by step</title>
    <script type="text/javascript" src="${basePath}/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" >
        $(function() {
            $('#submit').on('click',sendAjax);
        });
        function userInfo(username,password) {
            this.username = username;
            this.password = password;
        }
        function sendAjax(status) {
            console.info(status);
            var username = $('#username').val();
            var password = $('#password').val();
            var user = new userInfo(username,password);
            $.post(basePath+"/c6/getJsonString.do?t"+new Date().getTime()
                , {jsonStr:JSON.stringify(user)}
                ,function (data) {
                    $('#msg').val("Hello" + data.username + "!");
                });
            /*if(username!="" && password!=""){
                var user = new userInfo(username,password);
                $.post(basePath+"/c6/getJsonString.do?t"+new Date().getTime()
                    , {jsonStr:user}
                    ,function (data) {
                        $('#msg').val("Hello" + data.username + "!");
                    });
            }else{
                console.info("init click");
            }*/
        }
    </script>

</head>
<body>
<form > <!--action="c6/login.do" method="post" -->
    username:
    <input id="username" name="username" value=""/>
    <br/>
    password:
    <input id="password" name="password" type="password" value=""/>
    <br/>
    <input id="submit" name="submit" type="button" value="submit"/>
    <p id="msg"></p>
</form>

</body>
</html>